-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 23 2018 г., 04:22
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `service`
--

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `Наименование` char(150) NOT NULL,
  `Телефон` char(15) NOT NULL,
  `Почта` char(80) NOT NULL,
  `Дата` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Покупать` char(10) NOT NULL,
  `Основная продукция` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `Наименование`, `Телефон`, `Почта`, `Дата`, `Покупать`, `Основная продукция`) VALUES
(1, 'ООО \"Кит и Кот\"', '8(987) 112-4414', 'kitandkot@gmail.com', '2018-05-09 10:35:54', 'Да', 'Комлектующие для чайников'),
(2, 'ООО \"СамВсеСдел\"', '8(913) 817-8931', 'samsmogu@mail.ru', '2018-05-09 10:38:09', 'Да', 'Комплектующие для телефонов'),
(3, 'ИП Алексадр П.Р.', '8(912) 374-9114', 'alex293@mail.ru', '2018-05-09 10:41:53', 'Да', 'Материнские платы, процессоры, кулеры, видеокарты'),
(4, 'ИП Марат Ш.И.', '8(931) 344-5891', 'maratik@mail.ru', '2018-05-09 10:44:33', 'Нет', 'Матеирнские платы, процессоры, hdd'),
(5, 'ИП Альберт Ш.А.', '8(913) 499-9914', 'isikik@mail.ru', '2018-05-09 10:49:46', 'Да', 'Комплектующие для телефонов и ноутбуков'),
(6, 'И.П. Антон. А.Р.', '8(999) 131-8144', 'anton@mail.ru', '2018-05-26 11:43:12', 'Да', 'Комплектующие для элк. чайников, микроволновок');

-- --------------------------------------------------------

--
-- Структура таблицы `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `Наименование` varchar(255) DEFAULT NULL,
  `Артикул` varchar(233) DEFAULT NULL,
  `Цена` int(20) NOT NULL,
  `Остаток` varchar(11) DEFAULT NULL,
  `Необходимо` varchar(11) DEFAULT NULL,
  `Дата` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Поставщик` varchar(255) DEFAULT NULL,
  `Продается` varchar(233) DEFAULT NULL,
  `Покупать` varchar(233) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service`
--

INSERT INTO `service` (`id`, `Наименование`, `Артикул`, `Цена`, `Остаток`, `Необходимо`, `Дата`, `Поставщик`, `Продается`, `Покупать`) VALUES
(1, 'Материнская плата Asus 775', '775ASUS', 1200, '1', '4', '2018-05-09 11:01:31', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(2, 'Материнская плата Asus 1156', '1156ASUS', 2300, '1', '10', '2018-05-09 11:02:36', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(3, 'Материнская пата Gigabyte 775', '775GIGABYTE', 2300, '5', '10', '2018-05-09 11:45:01', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(4, 'Плата SCARLETT SC-1024', ' SC1024', 100, '2', '6', '2018-05-09 11:58:10', 'ООО \"Кит и Кот\"', 'Да', 'Да'),
(5, 'Процессор xeon x3440', 'X34401156', 1400, '6', '20', '2018-05-09 11:59:10', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(6, 'Процессор xeon x3450', 'X34501156', 1500, '3', '15', '2018-05-09 12:00:07', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(7, 'Процессор xeon e5450', 'E5450775', 1000, '3', '10', '2018-05-09 12:11:53', 'ИП Алексадр П.Р.', 'Да', 'Да'),
(8, 'Процессор xeon x5450', 'X5450775', 1100, '2', '10', '2018-05-09 12:45:53', 'ИП Алексадр П.Р.', 'Да', 'Да');

-- --------------------------------------------------------

--
-- Структура таблицы `zakaz`
--

CREATE TABLE `zakaz` (
  `id` int(11) NOT NULL,
  `ФИО` char(150) NOT NULL,
  `Телефон` char(15) NOT NULL,
  `Дата` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Тип проблемы` text NOT NULL,
  `Комплектующие` text NOT NULL,
  `Время` text NOT NULL,
  `Стоимость` int(10) DEFAULT NULL,
  `Ответственный` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zakaz`
--

INSERT INTO `zakaz` (`id`, `ФИО`, `Телефон`, `Дата`, `Тип проблемы`, `Комплектующие`, `Время`, `Стоимость`, `Ответственный`) VALUES
(1, 'Антон Петров', '8(999) 121-1115', '2018-05-09 13:08:22', 'короткое замыкание', 'Материнская плата Asus 775, ', '01:00', 1800, 'Николаев Антон');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `zakaz`
--
ALTER TABLE `zakaz`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `zakaz`
--
ALTER TABLE `zakaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
