﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LoginApp
{
    public partial class Form4 : Form
    {
        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        string t1 = GlobalParams.t1;
        string t2 = GlobalParams.t2;
        string t21 = GlobalParams.t21;
        string t3 = GlobalParams.t3;
        string t4 = GlobalParams.t4;
        string t5 = GlobalParams.t5;
        string t6 = GlobalParams.t6;
        string t7 = GlobalParams.t7;

        int ind1;
        public Form4(string setteble, int ind, string ind2, string ind3, string ind4, string ind5, string ind6, string ind7, string ind8, string ind9)
        {
            InitializeComponent();
            this.table = setteble;
            this.NameTextBox.Text = ind2;
            this.textBox1.Text = ind3;
            this.textBox2.Text = ind4;
            this.FromTextBox.Text = ind5;
            this.ToTextBox.Text = ind6;
            this.ComboBox.Text = ind7;
            this.comboBox1.Text = ind8;
            this.comboBox2.Text = ind9;
            ind1 = ind;
            try
            {
                string selectQuery = "SELECT * FROM `post` WHERE `Покупать` = 'Да';";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                MyReader2 = command.ExecuteReader();
                while (MyReader2.Read())
                {
                    this.ComboBox.Items.Add(MyReader2.GetString("Наименование"));
                }
                MyConn.Clone();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand upd3 = new MySqlCommand("SELECT COUNT(*) FROM `" + table + "` WHERE `" + t2 + "`='" + this.textBox1.Text + "';", MyConn2);
                MyConn2.Open();
                int ind;
                int.TryParse(upd3.ExecuteScalar().ToString(), out ind);
                int ind1, ind2;
                int.TryParse(this.FromTextBox.Text, out ind1);
                int.TryParse(this.ToTextBox.Text, out ind2);
                if ((ind1 <= ind2) && !((this.NameTextBox.Text == "") || (this.textBox1.Text == "") || (this.ComboBox.Text == "") || (this.comboBox1.Text == "") || (this.comboBox2.Text == "")))
                {
                    this.update();
                }
                else
                {
                    MessageBox.Show("Проверьте поля ввода");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Что то пошло не так, проверьте состояние БД");
            }
        }
        private void update()
        {
            try
            {
                string Query = "UPDATE " + table + " SET " + t1 + " ='" + this.NameTextBox.Text + "', " + t2 + " = '" + this.textBox1.Text + "', " + t21 + " = '" + this.textBox2.Text + "', " + t3 + "='" + this.FromTextBox.Text + "', " + t4 + " = '" + this.ToTextBox.Text + "', " + t5 + " = '" + this.ComboBox.Text + "', " + t6 + " = '" + this.comboBox1.Text + "', " + t7 + " = '" + this.comboBox2.Text + "' WHERE id='" + ind1 + "';";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {

                }
                MyConn2.Close();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}