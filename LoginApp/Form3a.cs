﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace LoginApp
{
    public partial class Form3a : Form
    {
        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        string t1 = GlobalParams.q1;
        string t2 = GlobalParams.q2;
        string t3 = GlobalParams.q3;
        string t4 = GlobalParams.q4;
        string t5 = GlobalParams.q5;
        string t6 = GlobalParams.q6;
        string pattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";
        public Form3a(string setteble)
        {
            InitializeComponent();
            this.table = setteble;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ((1 == 1) && !((this.NameTextBox.Text == "") || (this.textBox1.Text == "") || (this.ComboBox.Text == "") || (this.comboBox1.Text == ""))){
                    if (Regex.IsMatch(this.FromTextBox.Text, this.pattern, RegexOptions.IgnoreCase))
                    {
                        this.insert();
                    }
                    else
                    {
                        MessageBox.Show("Введите корректный адрес почты");
                    }
                }
                else{
                    MessageBox.Show("Проверьте поля вводы");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Что то пошло не так, проверьте состояние БД");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void insert()
        {
            try
            {
                string Query = "insert into `" + this.table + "` (`" + this.t1 + "`,`" + this.t2 + "`,`" + this.t3 + "`,`" + this.t5 + "`, `" + this.t6 + "`) values('" + this.NameTextBox.Text + "','" + this.textBox1.Text + "','" + this.FromTextBox.Text + "','" + this.ComboBox.Text + "','" + this.comboBox1.Text + "');";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {

                }
                MyConn2.Close();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Что то пошло не так, проверьте состояние БД");
            }
        }
    }
}
