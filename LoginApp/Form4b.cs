﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LoginApp
{
    public partial class Form4b : Form
    {
        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        string t1 = GlobalParams.w1;
        string t11 = GlobalParams.w11;
        string t2 = GlobalParams.w2;
        string t3 = GlobalParams.w3;
        string t4 = GlobalParams.w4;
        string t5 = GlobalParams.w5;
        string t6 = GlobalParams.w6;
        private int sum;
        private string s;
        private int mus = 0;

        int ind1;
        public Form4b(string setteble, int ind, string ind2, string ind3, string ind4, string ind5, string ind6, string ind7)
        {
            InitializeComponent();
            this.table = setteble;
            this.NameTextBox.Text = ind2;
            this.ComboBox.Text = ind3;
            this.textBox2.Text = ind5;
            this.textBox1.Text = ind6;
            this.comboBox1.Text = ind7;
            this.hochypirojok();
            ind1 = ind;
        }

        private void hochypirojok()
        {
            try
            {
                string selectQuery = "SELECT * FROM `service`;";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                MyReader2 = command.ExecuteReader();
                while (MyReader2.Read())
                {
                    this.checkedListBox1.Items.Add(MyReader2.GetString("Наименование"));
                }
                MyConn.Clone();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string xsq = "";
                foreach (object itemChecked in checkedListBox1.CheckedItems)
                {
                    string Query1 = "UPDATE `service` SET `Остаток` = `Остаток` -1 WHERE `Наименование` = '" + itemChecked + "';";
                    MySqlConnection MyConn21 = new MySqlConnection(connect);
                    MySqlCommand MyCommand21 = new MySqlCommand(Query1, MyConn21);
                    MySqlDataReader MyReader21;
                    MyConn21.Open();
                    MyReader21 = MyCommand21.ExecuteReader();
                    xsq += itemChecked + ", ";
                }
                string Query = "UPDATE " + table + " SET `" + t1 + "` ='" + this.NameTextBox.Text + "',`" + t11 + "` ='" + this.maskedTextBox1.Text + "', `" + t2 + "` = '" + this.ComboBox.Text + "', `" + t3 + "` = '" + xsq + "', `" + t4 + "` = '" + this.textBox2.Text + "', `" + t5 + "` = '" + this.textBox1.Text + "', `" + t6 + "` = '" + this.comboBox1.Text + "' WHERE id='" + ind1 + "';";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {

                }
                MyConn2.Close();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.price();
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.price();
        }

        private void textBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            this.price();
        }

        private void price()
        {
            mus = 0;
            sum = 0;
            foreach (object itemChecked in checkedListBox1.CheckedItems)
            {
                string selectQuery = "SELECT * FROM `service` WHERE `Наименование`= " + itemChecked + " ;";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                MyReader2 = command.ExecuteReader();
                while (MyReader2.Read())
                {
                    s = MyReader2.GetString("Цена");
                    int xs = Int32.Parse(s);
                    mus += xs;
                }
            }
            sum = mus;
            string xes = this.textBox2.Text;
            if (xes != "  :")
            {
                int x = Int32.Parse(xes.Remove(2));
                sum += x * 100 + 500;
            }
            this.textBox1.Text = sum.ToString();
        }
    }
}