﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LoginApp
{
    public partial class Form5 : Form
    {

        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        int obrobot;
        int i = 0;
        string[,] cool = new string[999, 2];
        private void updateits()
        {
            try
            {
                string selectQuery = "SELECT * FROM `service` WHERE `Продается` = 'Да';";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlCommand upd3 = new MySqlCommand("SELECT COUNT(*) FROM  `service` WHERE `Продается` = 'Да';", MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                int ind;
                int.TryParse(upd3.ExecuteScalar().ToString(), out ind);
                string[,] zlo = new string[ind, 2];
                MyReader2 = command.ExecuteReader();
                int sia = 0;
                int chebyrek = 0;
                while (MyReader2.Read())
                {
                    //наименование
                    Label newbutton = new Label() {
                        Text = MyReader2.GetString("Наименование"),
                        Width = 180,
                        Height = 40,
                        Location = new Point(30, 30 + sia)
                    };
                    this.Controls.Add(newbutton);
                    //уменьшение
                    int.TryParse(MyReader2.GetString("Остаток"), out obrobot);
                    NumericUpDown minus = new NumericUpDown() {
                        Text = "-",
                        Name = MyReader2.GetString("Наименование"),
                        Width = 50,
                        Height = 30,
                        Location = new Point(210, 25 + sia)
                    };
                    minus.Value = 0;
                    minus.Maximum = obrobot;
                    //minus.Click += new EventHandler(lolo);
                    this.Controls.Add(minus);

                    zlo[chebyrek, 0] = MyReader2.GetString("Наименование");
                    zlo[chebyrek, 1] = MyReader2.GetString("Остаток");

                    chebyrek++;
                    sia += 45;
                }
                cool = zlo;
                MyConn.Clone();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void lolo(object sender, EventArgs e)
        {
            var numeric = sender as NumericUpDown;
            //string _message = $"{numeric.Value} and {numeric.Name}";

        }

        public Form5()
        {
            InitializeComponent();
            updateits();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(cool);
            MessageBox.Show(json);
        }
    }
}
