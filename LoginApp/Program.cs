﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginApp
{
    public static class GlobalParams
    {
        public static string connect = "datasource=localhost;database=service;username=root;password=root";
        public static string table = "service";
        public static string parametr = "service";
        public static string t1 = "Наименование";
        public static string t2 = "Артикул";
        public static string t21 = "Цена";
        public static string t3 = "Остаток";
        public static string t4 = "Необходимо";
        public static string t5 = "Поставщик";
        public static string t6 = "Продается";
        public static string t7 = "Покупать";
        public static string q1 = "Наименование";
        public static string q2 = "Телефон";
        public static string q3 = "Почта";
        public static string q4 = "Дата";
        public static string q5 = "Покупать";
        public static string q6 = "Основная продукция";
        public static string w1 = "ФИО";
        public static string w11 = "Телефон";
        public static string w2 = "Тип проблемы";
        public static string w3 = "Комплектующие";
        public static string w4 = "Время";
        public static string w5 = "Стоимость";
        public static string w6 = "Ответственный";
    }

    static class obort
    {
        public static int[,] zlo;
    }

    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}