﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LoginApp
{
    public partial class Form2 : Form
    {

        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        string t1 = GlobalParams.t1;
        private int pr = 1;
        private void updateits()
        {
            try
            {
                string Query = "select * from " + table + ";";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void search(string searc)
        {
            try
            {
                string Query = "SELECT * FROM " + table + " WHERE `" + this.comboBox1.Text + "` LIKE'%"+searc+"%';";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Form2()
        {
            InitializeComponent();
            this.updateits();
            this.spisok_1();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (pr == 1)
                {
                    Form3 f3 = new Form3(this.table);
                    f3.ShowDialog();
                }
                else if(pr == 3)
                {
                    Form3b f3b = new Form3b(this.table);
                    f3b.ShowDialog();
                }
                else
                {
                    Form3a f3a = new Form3a(this.table);
                    f3a.ShowDialog();
                }
                this.updateits();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string ind1 = dataGridView1.SelectedCells[0].Value.ToString();
                string ind2 = dataGridView1.SelectedCells[1].Value.ToString();
                string ind3 = dataGridView1.SelectedCells[2].Value.ToString();
                string ind4 = dataGridView1.SelectedCells[3].Value.ToString();
                string ind5 = dataGridView1.SelectedCells[4].Value.ToString();
                string ind6 = dataGridView1.SelectedCells[6].Value.ToString();
                int ind;
                int.TryParse(ind1, out ind);
                if (pr == 1)
                {
                    ind6 = dataGridView1.SelectedCells[5].Value.ToString();
                    string ind7 = dataGridView1.SelectedCells[7].Value.ToString();
                    string ind8 = dataGridView1.SelectedCells[8].Value.ToString();
                    string ind9 = dataGridView1.SelectedCells[9].Value.ToString();
                    Form4 f4 = new Form4(this.table, ind, ind2 ,ind3 ,ind4, ind5, ind6, ind7, ind8, ind9);
                    f4.ShowDialog();
                }
                else if(pr == 3)
                {
                    ind3 = dataGridView1.SelectedCells[3].Value.ToString();
                    ind4 = dataGridView1.SelectedCells[4].Value.ToString();
                    ind5 = dataGridView1.SelectedCells[5].Value.ToString();
                    string ind7 = dataGridView1.SelectedCells[7].Value.ToString();
                    Form4b f4b = new Form4b(this.table, ind, ind2, ind3, ind4, ind5, ind6, ind7);
                    f4b.ShowDialog();
                }
                else
                {
                    ind5 = dataGridView1.SelectedCells[5].Value.ToString();
                    Form4a f4a = new Form4a(this.table, ind, ind2, ind3, ind4, ind5, ind6);
                    f4a.ShowDialog();
                }
                this.updateits();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int ind2 = dataGridView1.SelectedCells[0].RowIndex;
                string ind1 = dataGridView1.SelectedCells[0].Value.ToString();
                int ind;
                int.TryParse(ind1, out ind);
                dataGridView1.Rows.RemoveAt(ind2);
                string Query = "delete from " + table + " where id='" + ind + "';";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {

                }
                MyConn2.Close();
                this.updateits();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                this.updateits();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте состояние БД");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (pr == 1)
            {
                this.pr = 2;
                this.table = "post";
                button5.Text = "Поставщики";
                this.spisok_2();
            }
            else if(pr == 2)
            {
                this.pr = 3;
                this.table = "zakaz";
                button5.Text = "Заказы";
                this.spisok_3();
            }
            else 
            {
                this.pr = 1;
                this.table = "service";
                button5.Text = "Товары";
                this.spisok_1();
            }
            this.updateits();
        }

        private void spisok_1()
        {
            this.comboBox1.Items.Clear();
            this.comboBox1.Items.Add("Наименование");
            this.comboBox1.Items.Add("Артикул");
            this.comboBox1.Items.Add("Остаток");
            this.comboBox1.Items.Add("Необходимо");
            this.comboBox1.Items.Add("Поставщик");
        }

        private void spisok_2()
        {
            this.comboBox1.Items.Clear();
            this.comboBox1.Items.Add("Наименование");
            this.comboBox1.Items.Add("Телефон");
            this.comboBox1.Items.Add("Почта");
            this.comboBox1.Items.Add("Основная продукция");
        }

        private void spisok_3()
        {
            this.comboBox1.Items.Clear();
            this.comboBox1.Items.Add("ФИО");
            this.comboBox1.Items.Add("Тип проблемы");
            this.comboBox1.Items.Add("Комплектующие");
            this.comboBox1.Items.Add("Стоимость");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            search(textBox1.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            search(textBox1.Text);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            this.ExportToExcel();
        }

        private void ExportToExcel()
        {
            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "ExportedFromDatGrid";

                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGridView1.Columns[j].HeaderText;
                            cellRowIndex++;
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                            cellRowIndex = cellRowIndex - 1;

                        }
                        else
                        {
                            cellRowIndex++;
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                            cellRowIndex = cellRowIndex - 1;
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }

                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Export Successful");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }
    }
}