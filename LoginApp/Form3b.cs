﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace LoginApp
{
    public partial class Form3b : Form
    {
        string connect = GlobalParams.connect;
        string table = GlobalParams.table;
        string t1 = GlobalParams.w1;
        string t11 = GlobalParams.w11;
        string t2 = GlobalParams.w2;
        string t3 = GlobalParams.w3;
        string t4 = GlobalParams.w4;
        string t5 = GlobalParams.w5;
        string t6 = GlobalParams.w6;
        string pattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";
        private int sum;
        private string s;
        private int mus = 0;

        public Form3b(string setteble)
        {
            InitializeComponent();
            this.table = setteble;

        }

        private void Form3b_Load(object sender, EventArgs e)
        {
            try
            {
                string selectQuery = "SELECT * FROM `service` WHERE `Продается` = 'Да';";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                MyReader2 = command.ExecuteReader();
                while (MyReader2.Read())
                {
                    this.checkedListBox1.Items.Add(MyReader2.GetString("Наименование"));
                }
                MyConn.Clone();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ((1 == 1) && !((this.NameTextBox.Text == "") || (this.ComboBox.Text == "") || (this.comboBox1.Text == "") || (this.checkedListBox1.Text == "") || (this.textBox2.Text == "")))
                {
                        this.insert();
                }
                else{
                    MessageBox.Show("Проверьте поля вводы");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Что то пошло не так, проверьте состояние БД");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void insert()
        {
            try
            {
                string xsq = "";
                foreach (object itemChecked in checkedListBox1.CheckedItems)
                {
                    string Query1 = "UPDATE `service` SET `Остаток` = `Остаток` -1 WHERE `Наименование` = '" + itemChecked + "';";
                    MySqlConnection MyConn21 = new MySqlConnection(connect);
                    MySqlCommand MyCommand21 = new MySqlCommand(Query1, MyConn21);
                    MySqlDataReader MyReader21;
                    MyConn21.Open();
                    MyReader21 = MyCommand21.ExecuteReader();
                    xsq += itemChecked + ", ";
                }
                string Query = "insert into `" + this.table + "` (`" + this.t1 + "`,`" + this.t11 + "`,`" + this.t2 + "`,`" + this.t3 + "`,`" + this.t4 + "`,`" + this.t5 + "`, `" + this.t6 + "`) values('" + this.NameTextBox.Text + "','" + this.maskedTextBox1.Text + "','" + this.ComboBox.Text + "','" + xsq + "','" + this.textBox2.Text + "','" + this.textBox1.Text + "','" + this.comboBox1.Text + "');";
                MySqlConnection MyConn2 = new MySqlConnection(connect);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {

                }
                MyConn2.Close();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Что то пошло не так, проверьте состояние БД");
            }
        }

        private void checkedListBox1_ItemCheck(object sender, EventArgs e)
        {
            this.price();
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.price();
        }

        private void textBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            this.price();
        }

        private void price()
        {
            mus = 0;
            sum = 0;
            foreach (object itemChecked in checkedListBox1.CheckedItems)
            {
                string selectQuery = "SELECT * FROM `service` WHERE `Наименование`= '" + itemChecked + "' ;";
                MySqlConnection MyConn = new MySqlConnection(connect);
                MySqlCommand command = new MySqlCommand(selectQuery, MyConn);
                MySqlDataReader MyReader2;
                MyConn.Open();
                MyReader2 = command.ExecuteReader();
                while (MyReader2.Read())
                {
                    s = MyReader2.GetString("Цена");
                    int xs = Int32.Parse(s);
                    mus += xs;
                }
                MyConn.Close();
            }
            sum = mus;
            string xes = this.textBox2.Text;
            if (xes != "  :")
            {
                int x = Int32.Parse(xes.Remove(2));
                sum += x * 100 + 500;
            }
            this.textBox1.Text = sum.ToString();
        }
    }
}
